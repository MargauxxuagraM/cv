FROM node:latest as build

WORKDIR /app

COPY . /app

RUN npm i \
    && npm run build

FROM nginx:alpine

COPY --from=build /app/dist /usr/share/nginx/html
